# Kubernetes cert manager

Install cert manager on kubernetes cluster.

## Usage

```yaml
- name: Test role
  hosts: localhost
  vars:
    dns_email: kevin@keltio.fr
    kubernetes_cluster_path_kubeconfig: "{{ playbook_dir }}/kubeconfig.yaml"
    kubernetes_cert_manager_tmp_dir: /tmp/kubernetes/cert-manager
  roles:
  - role: kubernetes-cert-manager
```

## Vars
| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:-----:|
| dns_email | Email given to lestencrypt when issuing certificates | `string` | `` | yes |
| kubernetes_cert_manager_prod_cluster_issuer_name | Prod cluster issuer name | `string` | `letsencrypt-prod` | no |
| kubernetes_cert_manager_staging_cluster_issuer_name | Staging cluster issuer name | `string` | `letsencrypt-staging` | no |
| kubernetes_cluster_path_kubeconfig | Path where the cluster kubeconfig is located | `string` | `kubeconfig.yml` | no |
| kubernetes_cert_manager_tmp_dir | Path where the cert manager manifests will be created | `string` | `/tmp/kubernetes/cert-manager` | no |
| kubernetes_cert_manager_state | Decide if role install or uninstall cert manager | `Enum[present, absent]` | `present` | no |
